const list = [];

const form = document.querySelector(".tourist-attraction-form");

let image_input = document.querySelector(".open-file");

const image_label = document.querySelector(".tourist-attraction-form-image");

const title_input = document.querySelector(".tourist-attraction-form-title");
const desc_input = document.querySelector(".tourist-attraction-form-description");

document.addEventListener("DOMContentLoaded", function() {
    form.addEventListener("submit", addItemToList);
    image_input.addEventListener("change", loaderImage);
    const struct = document.querySelector('#items');
    let regEx = /[0-9a-zA-Z\^\&\'\@\{\}\[\]\,\$\=\!\-\#\(\)\.\%\+\~\_ ]+$/;

    function addItemToList(event) {
        event.preventDefault();
        
        // check the desc and title if ins't empty
        if(title_input.value === "")  return;
        else if(desc_input.value === "")    return;
        else {
            //let path = image_input.value.match(regEx);
            // const path = image_input.value;
            const ponto_turistico = {
                image: image_input,
                title: title_input.value,
                description: desc_input.value
            }
            
            localStorage.setItem('id', JSON.stringify(ponto_turistico));

            list.push(ponto_turistico);

            console.log(list);

            renderListItems();
            resetInputs();
        }
    }

    function loaderImage(event) {
        var reader = new FileReader();
        reader.onload = function() {
            image_label.style.backgroundImage = "url(" + reader.result + ")";
            image_input = reader.result;
            console.log((event.target.files[0].name));
        }
        reader.readAsDataURL(event.target.files[0]);
    }

    function renderListItems() {
        let itemsStructure = "";
        list.forEach(function(item) {
            itemsStructure += `
                <li>
                    <div class="card">
                        <img class="card-image" src="${item.image}" />
                        <div class="card-text-wrapper">
                            <h2 class="card-title">
                                ${item.title}
                            </h2>
                            <p class="card-description">
                                ${item.description}
                            </p>
                        </div>
                    </div>
                </li>
            `;
        });
        struct.innerHTML = itemsStructure;
    }

    function resetInputs() {
        image_label.style.backgroundImage = "url('')";
        image_input.value = '';
        title_input.value = '';
        desc_input.value = '';
    }

    function extractFilename(path) {
        if (path.substr(0, 12) == "C:\\fakepath\\")
            return path.substr(12); // modern browser
        var x;
        x = path.lastIndexOf('/');
        if (x >= 0) // Unix-based path
            return path.substr(x+1);
        x = path.lastIndexOf('\\');
        if (x >= 0) // Windows-based path
            return path.substr(x+1);
        return path; // just the filename
    }
});